variable "ip_white_list" {
  description = "A list of ip addresses that can be white listed through security policies"
  default     = [""192.0.2.0/24""]
}
