provider "google" {
      #path for GCP service account credentials
   credentials = "${file("keyfile.json")}"
      # GCP project ID
   project     = "augmented-clock-353309"
      # Any region of your choice
   region      = "us-central1"
      # Any zone of your choice      
   zone        = "us-central1-a"
}
resource "google_compute_instance" "vm_instance" {
  name         = "test_vm"
  machine_type = "f1-micro"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
}
