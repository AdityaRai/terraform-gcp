#VPC

resource "google_compute_network" "network" {
  name          = "my-network"
  auto_create_subnetworks = false
}

#vpc_subnetwork

resource "google_compute_subnetwork" "vpc_subnetwork" {
  name                     = "my-subnetwork"
  ip_cidr_range            = "10.2.0.0/16"
  region                   = "us-central1"
  network                  = google_compute_network.network.id
  private_ip_google_access = true
}

resource "google_compute_global_address" "default" {
  name          = "global-psconnect-ip"
  address_type  = "INTERNAL"
  purpose       = "PRIVATE_SERVICE_CONNECT"
  network       = google_compute_network.network.id
  address       = "100.100.100.106"
}

resource "google_compute_global_forwarding_rule" "default" {
  name          = "globalrule"
  target        = "all-apis"
  network       = google_compute_network.network.id
  ip_address    = google_compute_global_address.default.id
  load_balancing_scheme = ""  
}
resource "google_compute_instance" "default" {
  name         = "j-test"
  machine_type = "e2-medium"
  zone         = "us-central1-a"

  tags = ["foo", "bar", "web"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network     = google_compute_network.network.id
    subnetwork  = google_compute_subnetwork.vpc_subnetwork.id

  }

}

resource "google_compute_firewall" "default" {
  name    = "test-firewall"
  network = google_compute_network.network.id

  allow {
    protocol = "tcp"
    ports    = ["22", "3389"]
  }


  target_tags = ["web"]
}
resource "google_storage_bucket_access_control" "public_rule" {
  bucket = google_storage_bucket.default.name

  role   = "READER"
  entity = "allUsers"
}
resource "google_storage_bucket" "default" {
  name          = "j-bucket-007"
  location      = "US"
  force_destroy = true

  uniform_bucket_level_access = false
} 

resource "google_storage_object_access_control" "public_rule" {
  object = google_storage_bucket_object.object.name
  bucket = google_storage_bucket.default.name
  role   = "READER"
  entity = "allUsers"
}

resource "google_storage_bucket_object" "object" {
  name   = "public-object"
  bucket = google_storage_bucket.default.name
  source = "D:/terraform/Jefin/psc/sample.xml"
}

#On vm

#Verifying that the endpoint is working
#curl -v ENDPOINT_IP/generate_204
#curl -v 100.100.100.106/generate_204

#acces google storage api using  PRIVATE_SERVICE_CONNECT

#curl https://storage-ENDPOINT.p.googleapis.com/j-bucket-007/public-object

