#bucket 1

resource "google_storage_bucket" "bucket1" {
#Bucket name
 name = "jefin_test_bucket1"
labels = {
    key = "env" 
    value = "test2"
}
location = "us" 		#location type=multiregion
storage_class = "STANDARD"
uniform_bucket_level_access = true

#retension policy:The period of time, in seconds, that objects in the bucket must be retained and cannot be deleted, overwritten, or archived. The value must be less than 2,147,483,647 seconds.

retention_policy {
  retention_period = 30 #IN SECONDS
  is_locked = true
}
}

resource "google_storage_bucket_object" "test-text2" {
  name   = "test_file"
  content = "Hi World"
  bucket = "${google_storage_bucket.bucket1.name}"
}


#VM Instance 1

resource "google_compute_instance" "vm_instance" {
  name         = "test"
  machine_type = "f1-micro"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
}


#bucket 2


resource "google_storage_bucket" "bucket2" {
#Bucket name
 name = "jefin_test_bucket2"
labels = {
    key = "env" 
    value = "test2"
}
location = "us" 		#location type=multiregion
storage_class = "STANDARD"
uniform_bucket_level_access = true
}







