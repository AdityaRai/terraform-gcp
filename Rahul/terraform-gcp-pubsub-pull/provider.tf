provider "google" {
 project = "augmented-clock-353309"
 region = "us-east1-b"
 credentials = "${file("cred.json")}"
}
