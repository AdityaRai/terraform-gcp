terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.27.0"
    }
  }
}

provider "google" {
 credentials = "${file("cred.json")}"
 project     = "augmented-clock-353309"
 region      = "us-central1"
 zone        = "us-central1-a"
}
