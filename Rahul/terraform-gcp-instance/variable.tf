variable "project_id" {
  type = string
  default = "augmented-clock-353309"
  description = "Pelase mention your proejct ID"
}
variable "zone" {
  type = string
  default = "us-east1-b"
  description = "Please mention zone default zone is us-east1-b"
}
variable "region" {
  type = string
  default = "us-east1"
  description = "Please mention region for your resource default is us-east1"
}
variable "machine_type" {
  type = string
  default = "f1-micro"
  description = "Mention machine type default is f1-micro"
}
variable "gce_name" {
  type = string
  default = "rah-prac"
  description = "Name of resource"
}
variable "image" {
  type = string
  default = "debian-cloud/debian-9"
  description = "Compute instance image"
}
variable "project_network" {
  type = string
  default = "default"
  description = "Network we will using for compute instance creation"
}
variable "credentials" {
  type = string
  default = "cred.json"
  description = "service account credentials"
}
