terraform {
  backend "gcs" {
    bucket = "rahul-test-demo"
    prefix = "terraform/tfstat"
    credentials = "cred.json"
  }
}
