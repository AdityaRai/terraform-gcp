provider "google" {
 credentials = "${file("cred.json")}"
 project     = "augmented-clock-353309"
 region      = "us-east1"
 zone        = "us-east1-b"
}
