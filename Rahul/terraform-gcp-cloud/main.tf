resource "google_storage_bucket" "gcs" {
 count = 1
 name = var.cloud_bucket_name
 location = var.region
 storage_class = var.storage_class
}
resource "google_storage_bucket_object" "picture" {
 name = "googleimage"
 bucket = "${google_storage_bucket.gcs[0].name}"
 source = var.image
}

