variable "project_id" {
  type = string
  default = "augmented-clock-353309"
  description = "Pelase mention your proejct ID"
}
variable "zone" {
  type = string
  default = "us-east1-b"
  description = "Please mention zone default zone is us-east1-b"
}
variable "region" {
  type = string
  default = "us-east1"
  description = "Please mention region for your resource default is us-east1"
}
variable "gce_name" {
  type = string
  default = "rah-prac"
  description = "Name of resource"
}
variable "credentials" {
  type = string
  default = "cred.json"
  description = "service account credentials"
}
variable "cloud_bucket_name" {
  type = string
  default = "terraformbucketrah"
  description = "the name of the cloud bucket"
}
variable "storage_class" {
  type = string
  default = "STANDARD"
  description = "the storage class is set to STANDARD by default"
}
variable "image" {
  type = any
  default = "pic.jpg"
  description = "the name of the image"
}
