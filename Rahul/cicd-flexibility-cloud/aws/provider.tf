provider "google" {
 credentials = "${file("cred.json")}"
 project     = "augmented-clock-353309"
 region      = "us-central1"
 zone        = "us-central1-a"
}