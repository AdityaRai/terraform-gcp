resource "google_compute_instance" "vm_instance" {
  #A unique name for the resource
  name         = "rah-practice"
  machine_type = "f1-micro"
  boot_disk {
    initialize_params {
      #Image name
      image = "debian-cloud/debian-9"
    }
  }
  #Networks to attach to the instance
  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
}