provider "google" {
      #path for GCP service account credentials
   credentials = var.credentials
      # GCP project ID
   project     = var.project
      # Any region of your choice
   region      = var.region
      # Any zone of your choice      
   zone        = var.zone
}
