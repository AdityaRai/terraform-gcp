variable "project" {
  description = "The ID of the google project to which the resource belongs. If it is not provided, the provider project is used."
  type = string
  default     = "augmented-clock-353309"
}

variable "location" {
  description = "Bucket location."
  type        = string
  default     = "europe-west2"
}

variable "credentials" {
  type = string
  default = "keyfile.json"
  description = "service account credentials"
}


variable "storage_class" {
  description = "Bucket storage class."
  type        = string
  default     = "STANDARD"
}  

variable "versioning" {
  description = "Version bucket objects?"
  default     = false
}
variable "region" {
  description = "The GCS region. If it is not provided, the provider region is used."
  default     = "us-central1"
}
variable "zone" {
  description = "The GCS region. If it is not provided, the provider region is used."
  default     = "us-central1-a"
}
