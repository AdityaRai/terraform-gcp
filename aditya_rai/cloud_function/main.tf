resource "google_storage_bucket" "bucket" {
#Number of bucket
 count = 1
#Bucket name
 name = "adityabucket01"
# Any location of your choice
 location = "europe-west2"
# Any storage_class of your choice
 storage_class = "STANDARD"
}

resource "google_storage_bucket_object" "archive" {
  name   = "index.zip"
  bucket = "${google_storage_bucket.bucket[0].name}"
  source = "index.zip"
}

resource "google_cloudfunctions_function" "function" {
  name        = "aditya-function-test"
  description = "My function"
  runtime     = "nodejs16"

  available_memory_mb   = 128
  source_archive_bucket = "${google_storage_bucket.bucket[0].name}"
  source_archive_object = "${google_storage_bucket_object.archive.name}"
  trigger_http          = true
  entry_point           = "helloWorld"
}
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}